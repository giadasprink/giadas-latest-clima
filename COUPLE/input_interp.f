     
      SUBROUTINE INPUT_INTERP(temp_alt, water, O3, Jcold, T, FI)
C-KK This subroutine interpolates the (alt),water, ozone file that is READ in 
C-KK from atm_chem.
!FI is matrix of mixing ratios
!strip down to be simpler so that it takes in only one gas concentration
!spits out profile of gas concentration
!FI_temp = one dimensional vector -> manually set the particular column of FI
!grid to the values in that vector
!CALL INPUT_INTERP(water, FI's column for water)

      INCLUDE '../header.inc'
      PARAMETER (NZ=64)
      DIMENSION O3(NZ), water(NZ), temp_alt(NZ),T(ND),FI(4,ND)
      CHARACTER :: DIRINOUT*2,DIRDATA*4
      COMMON/DIR/DIRINOUT,DIRDATA
      COMMON/ALTBLOK/DALT(ND-1),RADIUS(ND-1),PARTICLES(ND),RAER(ND),
     & 		     ALT(ND)
      COMMON/PRESSURE/P(ND),PLOG(ND)

C-KK   both indices starting at ground level
	istart = 1
	j = ND
	FI(4,j) = O3(istart)

	istart = istart+1
	top = temp_alt(NZ)
	
	DO j = ND-1, 1, -1 !starting at ND-1, which is second to last layer, which in climate code is the surface 
           !clima and photochem alt codes are reversed from each other (counting by increments of -1)
	 DO i = istart, NZ
	   IS = i
	   if (temp_alt(i) .GT. ALT(j)) GOTO 350
	 END DO
  350  CONTINUE
	 IS1 = IS-1
 	 istart = IS1
	 IF (IS .GT. NZ) GOTO 471
	 DZI = temp_alt(IS)-temp_alt(IS1)
	 DZJ = ALT(j) - temp_alt(IS1)
	 FR = DZJ/DZI
	 IF (ALT(j) .LT. top) THEN !is this a bug that this layer logic is not built into the water? (top = top of photochem grid)
 	   O3log1 = ALOG(O3(IS1))
	   O3log2 = ALOG(O3(IS))
	   O3log = (FR*O3log2) + ((1-FR)*O3log1)	
	  ELSE O3log = FI(4,j+1)
	 END IF	
	 FI(4,j) = EXP(O3log)
 	 Flog1 = ALOG(water(IS1))
	 Flog2 = ALOG(water(IS))
	 flog = (FR*Flog2) + ((1-FR)*Flog1)
	 FI(1,j) = EXP(flog)
	END DO
  471  CONTINUE

	DO k =1, ND !k is index; looping over number of layers in climate code (ND = number of layers in clima)
           !goes from top of atmosphere since clima counts from top down
	  if (ALT(k) .GE. temp_alt(JCOLD)) jcold_new = k !redefines cold trap until it matches the real cold trap
 	END DO
	
	JCOLD = jcold_new

        !take out water statements and generalize the o3 statement to be for any gas.  
        !Will interpolate that specific gas to the climate code.  
        !We could probably use the same function to interpolate the number density of the particles.  
        !Then we can call this a bunch of times for each thing we're interested in.

	RETURN
        END
