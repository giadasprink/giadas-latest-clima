First, we have to run Forward calculations for Mars model to find a stratospheric temperature from 
first condensation of CO2. Here are the inputs & how it is done:



input_climat.dat:
-----------------
NSTEPS=    600       !step number (200 recommended for coupling)
IMW=       0
RSURF=     0.8
ZY=        60.
DTAU0=     0.5
ZCON=      20
P0=        5.e-5
PG0=       6.0e-3    ! PG0 is the current surface pressure on Mars. If PG0 is used, appropriate 
'Clima.f' needs to be used to find FCO2
G=         373.      !Gravity (Mars=373., Earth=980., Gliese581d=1744.0, assuming 5.6 earth 
masses)
FAC=       4.0
IO3=       0                    !Ozone?
IUP=       0                    !1 means that it starts a new run and doesn't read TempIn.dat
TG0=       210.                 !Surface temperature for IUP=1
TSTRAT=    167.                 !Stratospheric temperature for IUP=1
STARR=     "Sun"                !What star?
ICONSERV=  1                    !Type of energy conservation
ICOUPLE=   0                    !Coupled(1) or not(0)
SRFALB=    0.22                 !fixed planetary albedo (0.2)
SOLCON=    0.43         !SOLCON=S/So (0.3225 = 0.75*0.43)  use 0.43 for present day mars, .2844 for 
GJ581d,22.49 unscaled
dtmax=     1.e4
CO2MAX=    3.55e-2
IMET=      0                    ! Methane flag. If "1" then methane is turned on.
nga=       1

mixing_ratios.dat:
------------------
 1.000E-60          !Argon
 1.000E-60          !Methane
 1.000E-60          !Ethane
 0.950E-00          !Carbon Dioxide
 1.00               !Nitrogen
 1.000E-60          !Oxygen
 1.000E-60          !Hydrogen
 1.000E-60          !Nitrogen Dioxide
 13                 !Tropopause layer

Steps to do Forward calculations:
---------------------------------
1. Change PG0 from 6e-3 to 1 bar or so in steps of your choosing with options above.
2. Run couple of hundred steps (I chose 600) until you reach convergence. Convergence means   
   that you need DIVFrms & DT(ND) in 'clima_allout.tab' to be very small (typically ~ 1e-4).
3. Once you have convergence, make a copy of 'clima_allout.tab' and 'clima_last.tab' for that 
   particular PG0.
4. Copy 'TempOut.dat' to 'TempIn.dat'. Change PG0 to the next value. Run again for couple 
   hundred steps until convergence.
5. Usually you repeat steps 2 to 4. But once you have two or three converged runs, plot 
   'PSATCO2/P' versus altitude from 'clima_last.tab' files. Notice which curve touches a 
    value of '1' on the x-axis. That is the onset of CO2 condenstation (because that is the place 
    where the saturation vapor pressure of CO2 = ambient pressure). For our case that happens 
    for PG0 = 0.69.
6. Plot Temperature profile (Temperature versus altitude) for PG0=0.69 or the condensation 
   curve that you get. Choose  the cold-trap temperature ("tstrat") at the altitude where 
   PSATCO2/P = 1. Use this temperature ("tstrat") to approximate a constant stratospheric 
   temperature (for our case, that is 154 K, at 40 km)
7. Take this stratospheric temperature ("tstrat") and also note the planetary albedo (ALBP)
   Calculate Seff = Seffo*(1-ALBP)*SOLCON, where: Seffo is the FTIR/FTSOL at the top of atmosphere for the this 
   condensation run. Seffo (should be close to one for converged forward calculation).
8. This "Seff" is the "Tconstant" that goes into the denominator of the following expression (in Clima.f):
   TSTRAT = tstrat*((SEFF*(1.0-ALBP))/TCONST)**0.25
   And then you can iterate until converge on a TSTRAT solution.



